import Cocoa
/*
let compose = {(a:(Double -> Double),b:(Double -> Double)) -> (Double -> Double) in
               {(x:Double) -> Double in a(b(x))}}
let powc = {(a:Double) -> Double -> Double in 
            {(b:Double) -> Double in pow(a,b)}}
let mul = {(a:Double) -> Double -> Double in {(b:Double) -> Double in a*b}}
let div = {(a:Double) -> Double -> Double in {(b:Double) -> Double in a/b}}
let fib = compose(round,
                  compose(((mul) (1.0/pow(5.0,0.5))),powc((1.0+pow(5.0,0.5))/2.0)))
*/

//MATERIALS

// part 1

// fiblst returns an array of the first n fibonacci numbers.  Instead of filling another
// array, this just returns an array, which can be appended to the array to be filled
// if that is needed.

func fiblst(n:Int) -> [Int] {
  return (2..<n).reduce([1,1], combine:{(a,b) -> [Int] in a+[a[b-1]+a[b-2]]}) }
var n : [Int] = fiblst(20)
print(fiblst(20))

// part 2

class Student : CustomStringConvertible { var name : String; var favn : UInt32
  init(n:String, f:UInt32) { name = n; favn = f; } 
  var description : String { return "name: \(name); favorite-number: \(favn)!" } }

var several_students : [Student] = []

func strstudent(x:String...) -> [Student] {
  return x.map({(a) -> Student in Student(n:a,f:arc4random_uniform(21))}) }

several_students += strstudent("Name1","Name2","Name3")
for s : Student in several_students { print(s) }

// part 3

class Vehicle { var wheels : Int; var model : String; var price : Int
  init(w:Int,m:String,p:Int) { wheels = w; model = m; price = p }
  func describe() { print("The \(model) has a price of $\(price).") }
  func tspeed() { print("The \(model) has a top speed of \(price/wheels)") } } 

class Motorcycle : Vehicle {
  override func describe() { super.describe(); print("Always wear a helmet.") } }

typealias Car = Vehicle

Motorcycle(w:2,m:"eeeeee",p:50000).describe()
var car = Car(w:4,m:"ffffff",p:100000)
car.describe(); car.tspeed()
  